<?php
    include 'layouts/header.php';
    include 'lib/Apple.php';
    $apple = new Apple();
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['upload'])) {
        $apple->clearDatabase();
        for ($i = 0; $i<100; $i++) {
            $arrival = $apple->randomDate("2017-01-01", "2017-06-31");
            $sold = $apple->randomDate($arrival, "2017-06-31");
            $price = $apple->randomPrice(1000, 4000);
            $discount = $apple->randomDiscount(1, 10);
            $apples = $apple->uploadDatabase($arrival, $sold, $price, $discount);
        }
    }
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h2>Nyilvántartás</h2>
    </div>
    <div class="panel-body">
        <?php
            if (isset($apples)) {
                echo $apples;
            }
        ?>
        <form method="post" action="">
            <input type="submit" name="upload" value="Adatbázis újratöltése" class="btn btn-success">
        </form>

        <h2>A 10 leghosszabb ideig raktárban lévő almás láda adatai</h2>
        <table>
            <tr>
                <th>Raktárban töltött napok száma</th>
                <th>Raktárba érkezés dátuma</th>
                <th>Eladás dátuma</th>
                <th>Normál ár</th>
                <th>Akciós engedmény</th>
            </tr>
        <?php $apple->longestTimeInStorage(); ?>
        </table>

        <hr>

        <h2> A 10 legnagyobb Ft engedményért (nem %) értékesített almás láda adatai</h2>
        <table>
            <tr>
                <th>Raktárba érkezés dátuma</th>
                <th>Eladás dátuma</th>
                <th>Normál ár</th>
                <th>Akciós árengedmény</th>
                <th>Árengedmény</th>
            </tr>
            <?php $apple->biggestDiscounts(); ?>
        </table>

        <hr>

        <h2>Eladások száma és összege havonta csoportosítva</h2>
        <table>
            <tr>
                <th>Hónap</th>
                <th>Eladások száma</th>
                <th>Eladások összege</th>
            </tr>
            <?php $apple->sumSalesAndPriceByMonth(); ?>
        </table>

    </div>
</div>

<?php
    include 'layouts/footer.php';
?>
