<?php

class Db {

    private $hostname = "localhost";
    private $username = "root";
    private $password = "";
    private $database = "db_nwmgroup_2";
    public $pdo;

    public function __construct()
    {
        if (!isset($this->pdo)) {
            try{
                $link = new PDO(
                    "mysql:host=".$this->hostname.";
                     dbname=".$this->database, $this->username, $this->password
                );
                $link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $link->exec("SET CHARACTER SET UTF8");

                $this->pdo = $link;

            }catch (PDOException $e){
                die("Nem sikerült csatlakozni az adatbázishoz: " . $e->getMessage());
            }
        }
    }
}