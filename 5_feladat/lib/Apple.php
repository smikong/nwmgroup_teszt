<?php

include 'Db.php';

class Apple {

    private $db;

    public function __construct()
    {
        $this->db = new Db();
    }

    public function randomDate($startDate, $endDate, $format = 'Y-m-d')
    {
        $min = strtotime($startDate);
        $max = strtotime($endDate);

        $result = mt_rand($min, $max);

        return date($format, $result);
    }

    public function randomPrice($minPrice, $maxPrice)
    {
        $result = mt_rand($minPrice, $maxPrice);

        return $result;
    }

    public function randomDiscount($minDiscount, $maxDiscount)
    {
        $result = mt_rand($minDiscount, $maxDiscount);

        return $result;
    }

    public function uploadDatabase($arrival, $sold, $price, $discount)
    {
        $sql = "INSERT INTO sold_boxes (arrival, sold, price, discount) VALUES('$arrival', '$sold', $price, $discount)";
        $query = $this->db->pdo->prepare($sql);
        $result = $query->execute();

        if ($result) {
            $message = "<div class='alert alert-success text-center'><strong>Sikeres adatfeltöltés!</strong></div>";
            return $message;
        } else {
            $message = "<div class='alert alert-danger text-center'>Hiba! <strong>Sikertelen adatfeltöltés!</strong></div>";
            return $message;
        }
    }

    public function clearDatabase()
    {
        return $this->db->pdo->query("TRUNCATE TABLE sold_boxes");
    }

    public function longestTimeInStorage()
    {
        $sql = 'SELECT * FROM sold_boxes ORDER BY (arrival-sold) LIMIT 10';
        foreach ($this->db->pdo->query($sql) as $row) {
            echo '<tr>';
            echo '<td>' . date_diff(date_create($row['arrival']), date_create($row['sold']))->days . '</td>';
            echo '<td>' . $row['arrival'] . '</td>';
            echo '<td>' . $row['sold'] . '</td>';
            echo '<td>' . $row['price'] . ' Ft</td>';
            echo '<td>' . $row['discount'] . " %</td>";
            echo '</tr>';
        }
    }

    public function biggestDiscounts()
    {
        $sql = 'SELECT * FROM sold_boxes ORDER BY (price/100*discount) DESC LIMIT 10';
        foreach ($this->db->pdo->query($sql) as $row) {
            echo '<tr>';
            echo '<td>' . $row['arrival'] . '</td>';
            echo '<td>' . $row['sold'] . '</td>';
            echo '<td>' . $row['price'] . ' Ft</td>';
            echo '<td>' . $row['discount'] . " %</td>";
            echo '<td>' . $row['price']/100*$row['discount'] . ' Ft</td>';
            echo '</tr>';
        }
    }

    public function sumSalesAndPriceByMonth()
    {

        $sql = 'SELECT MONTH(sold) as month, SUM(price) as price, COUNT(sold) as number FROM sold_boxes GROUP BY MONTH(sold) ORDER BY MONTH(sold)';
        foreach ($this->db->pdo->query($sql) as $row) {
            echo '<tr>';
            echo '<td>' . $row['month'] . '</td>';
            echo '<td>' . $row['number'] . '</td>';
            echo '<td>' . $row['price'] . '</td>';
            echo '</tr>';
        }
    }

}
