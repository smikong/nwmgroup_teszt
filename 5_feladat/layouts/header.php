<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Feladat 5</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="index.php">Feladat 5<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="../1_feladat/index.php">Feladat 1</a></li>
                            <li><a href="../2_feladat/index.php">Feladat 2</a></li>
                            <li><a href="../3_feladat/index.php">Feladat 3</a></li>
                            <li><a href="../4_feladat/index.php">Feladat 4</a></li>
                            <li class="active"><a href="index.php">Feladat 5</a></li>
                            <li><a href="../6_feladat/index.php">Feladat 6</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>