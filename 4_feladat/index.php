<?php
    include 'layouts/header.php';
    include 'imageUpload.php';
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h2>Képfeltöltés</h2>
    </div>

    <div class="panel-body">
        <?php
            if (isset($message)) {
                echo $message;
            }
        ?>
        <form method="post" enctype="multipart/form-data">
            <input type="file" name="image_file" class="center-block">
            <br>
            <input type="submit" name="submit" value="Feltöltés" class="center-block btn btn-primary">
        </form>
    </div>
</div>

<?php
    include 'layouts/footer.php';
?>