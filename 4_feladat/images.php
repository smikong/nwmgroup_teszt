<?php
include 'layouts/header.php';
?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>Feltöltött képek</h2>
            <a href="index.php" class="btn btn-success"><span>+ </span>Új kép feltöltése</a>
        </div>
        <?php
        if(isset($_GET['success'])){
            echo "<div class='alert alert-success text-center'>Sikeres képfeltöltés!</div>";
        }
        ?>
        <div class="panel-body">
            <div class="col-md-12">
            <?php
                include 'imagesShow.php';
            ?>
            </div>
        </div>
    </div>

<?php
include 'layouts/footer.php';
?>