<?php
if(isset($_FILES['image_file']))
{
    $max_size = 600;
    $destination_folder = realpath('').'/images';
    $watermark_png_file = 'watermark.png';

    $countImages = count(glob("images/*.*"));
    $image_name = 'image_' . $countImages . '.png';
    $image_size = $_FILES['image_file']['size'];
    $image_temp = $_FILES['image_file']['tmp_name'];
    $image_type = $_FILES['image_file']['type'];

    switch(strtolower($image_type)){
        case 'image/png':
            $image_resource =  imagecreatefrompng($image_temp);
            break;
        case 'image/gif':
            $image_resource =  imagecreatefromgif($image_temp);
            break;
        case 'image/jpeg':
            $image_resource = imagecreatefromjpeg($image_temp);
            break;
        default:
            $image_resource = false;
            $message = "<div class='alert alert-danger text-center'><strong>Hiba! </strong>Nem megfelelő képformátum!</div>";
    }

    if($image_resource){
        list($img_width, $img_height) = getimagesize($image_temp);

        $image_scale = min($max_size / $img_width, $max_size / $img_height);
        $new_image_width = ceil($image_scale * $img_width);
        $new_image_height = ceil($image_scale * $img_height);
        $new_canvas = imagecreatetruecolor($new_image_width , $new_image_height);

        if(imagecopyresampled($new_canvas, $image_resource , 0, 0, 0, 0, $new_image_width, $new_image_height, $img_width, $img_height))
        {

            if(!is_dir($destination_folder)){
                mkdir($destination_folder);
            }

            $watermark_left = ($new_image_width/2);
            $watermark_bottom = ($new_image_height/2);

            $watermark = imagecreatefrompng($watermark_png_file);
            imagecopy($new_canvas, $watermark, $watermark_left, $watermark_bottom, 0, 0, 300, 100); //merge image

            imagejpeg($new_canvas, $destination_folder.'/'.$image_name , 90);
            header("Location: images.php?success");

            imagedestroy($new_canvas);
            imagedestroy($image_resource);
            die();
        }
    }
}