<?php
$images = glob("images/*.*");
$modalId = 0;
foreach ($images as $image) {
    $modalId++;
    echo '<img src="' . $image . '" class="image-list" data-toggle="modal" data-target="#' . $modalId . '">
            <div class="modal fade" id="' . $modalId . '" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">x</button>
                        </div>
                        <div class="modal-body">
                            <img class="img-responsive center-block" src="' . $image . '">
                        </div>
                    </div>
                </div>
            </div>';
}