<?php
    include 'layouts/header.php';
    include 'visitorCounter.php';
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h2>Számláló</h2>
    </div>
    <div class="panel-body text-center">
        <h1>Látogatók száma: <?php echo $visitors; ?></h1>

        <form action="downloadCounter.php" method="post">
            <input type="submit" value="Letöltés" class="btn btn-primary">
        </form>

        <h1>Letöltések száma: </h1>
        <div class="center-block">
            <?php
                $downloads = file_get_contents('downloads.txt');
                $downloads = str_pad($downloads, 5, "0", STR_PAD_LEFT);
                $chars = preg_split('//', $downloads);

                foreach ($chars as $key => $char) {
                     if (0<$key && $key<6) {
                        echo '<img class="number-image" src="images/num_'.$char.'.png">';
                    }
                }
            ?>
        </div>
    </div>
</div>

<?php
    include 'layouts/footer.php';
?>