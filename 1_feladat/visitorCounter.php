<?php
session_start();

$file = "visitors.txt";

if (!file_exists($file)) {
    $f = fopen($file, "w");
    fwrite($f,"0");
    fclose($f);
}

$f = fopen($file,"r");
$visitors = fread($f, filesize($file));
fclose($f);

if(!isset($_SESSION['visited'])){
    $_SESSION['visited']="yes";
    $visitors++;
    $f = fopen($file, "w");
    fwrite($f, $visitors);
    fclose($f);
}