<?php
    include 'layouts/header.php';
    include 'convert.php';

    $error_message = "";
    $fullResult = "";
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h2>Deviza átváltó</h2>
    </div>
    <div class="panel-body text-center">
        <form action="" method="post" class="form-group">

            <div class="col-md-12">
                <h2>Forrás deviza:</h2>
                <label>
                    <select name="from">
                        <option value="HUF">Forint (HUF)</option>
                        <option value="EUR">Euro (EUR)</option>
                        <option value="USD">Dollár (USD)</option>
                    </select>
                </label>
            </div>

            <div class="col-md-12">
                <h2>Cél deviza:</h2>
                <label>
                    <select name="to">
                        <option value="HUF">Forint (HUF)</option>
                        <option value="EUR" selected>Euro (EUR)</option>
                        <option value="USD">Dollár (USD)</option>
                    </select>
                </label>
            </div>

            <div class="col-md-12">
                <h3>Összeg:</h3>
                <label>
                    <input type="number" name="amount" required>
                </label>
            </div>

            <br>

            <div class="col-md-12">
                <label>
                    <input type="submit" name="submit" class="btn btn-primary" value="Átváltás">
                </label>
            </div>

        </form>

        <?php require 'errors.php'; ?>
        <?php require 'result.php'; ?>

        <?php
            if(!empty($error_message)) {
                echo '<h4 style="color:red;">' . $error_message . '</h4>';
            } else {
                echo '<h3>' . $fullResult . '</h3>';
            }
        ?>
    </div>
</div>

<?php
    include 'layouts/footer.php';
?>