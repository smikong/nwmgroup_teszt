<?php
    include 'layouts/header.php';
    include 'lib/User.php';
    $user = new User();
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['register'])) {
        $registerUser = $user->register($_POST);
    }
?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>Regisztráció</h2>
        </div>

        <div class="panel-body" style="max-width: 600px;margin: 0 auto;">
            <?php if (isset($registerUser)) {
                    echo $registerUser;
                }
            ?>
            <form action="" method="post">

                <div class="form-group">
                    <label for="email">E-mail cím</label>
                    <input type="text" id="email" name="email" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="name">Név</label>
                    <input type="text" id="name" name="name" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="password">Jelszó</label>
                    <input type="password" id="password" name="password" class="form-control" required>
                </div>

                <button type="submit" name="register" class="btn btn-success center-block">Regisztráció</button>
            </form>
        </div>
    </div>

<?php
    include 'layouts/footer.php';
?>