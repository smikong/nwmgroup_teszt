<?php
    $filePath = realpath(dirname(__FILE__));
    include_once $filePath.'/../lib/Session.php';
    Session::init();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Feladat 3</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <?php
    if (isset($_GET['action']) && $_GET['action'] == 'logout') {
        Session::destroy();
    }
    ?>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="index.php">Feladat 3 <span class="caret"></span></a>
                        <ul class="dropdown-menu">

                            <li><a href="../1_feladat/index.php">Feladat 1</a></li>
                            <li><a href="../2_feladat/index.php">Feladat 2</a></li>
                            <li class="active"><a href="../3_feladat/index.php">Feladat 3</a></li>
                            <li><a href="../4_feladat/index.php">Feladat 4</a></li>
                            <li><a href="../5_feladat/index.php">Feladat 5</a></li>
                            <li><a href="../6_feladat/index.php">Feladat 6</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <ul class="nav navbar-nav pull-right">

                <?php
                    if (Session::get('login') == true) {
                ?>
                    <li><a href="?action=logout">Kijelentkezés</a></li>
                <?php
                    } else {
                ?>
                    <li><a href="login.php">Bejelentkezés</a></li>
                    <li><a href="register.php">Regisztráció</a></li>
                <?php
                    }
                ?>

            </ul>
        </div>
    </nav>