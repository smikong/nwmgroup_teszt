<?php

include_once 'Session.php';
include 'Database.php';

class User {

    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function register(array $data)
    {
        $email = $data['email'];
        $name = $data['name'];
        $password = md5($data['password']);

        $checkEmail = $this->checkEmail($email);

        if (strlen($name) < 3) {
            $message = "<div class='alert alert-danger text-center'><strong>Hiba! </strong>A név túl rövid!</div>";
            return $message;
        }

        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $message = "<div class='alert alert-danger text-center'><strong>Hiba! </strong>Nem megfelelő e-mail formátum!</div>";
            return $message;
        }

        if($checkEmail) {
            $message = "<div class='alert alert-danger text-center'><strong>Hiba! </strong>Ezzel az e-mail címmel már regisztráltak!</div>";
            return $message;
        }

        $sql = "INSERT INTO users (email, name, password) VALUES(:email, :name, :password)";
        $query = $this->db->pdo->prepare($sql);
        $query->bindValue(':email', $email);
        $query->bindValue(':name', $name);
        $query->bindValue(':password', $password);
        $result = $query->execute();

        if ($result) {
            $message = "<div class='alert alert-success text-center'><strong>Sikeres regisztráció!</strong></div>";
            return $message;
        } else {
            $message = "<div class='alert alert-danger text-center'>Hiba! <strong>Sikertelen adatfeltöltés!</strong></div>";
            return $message;
        }

    }

    public function checkEmail(string $email)
    {
        $sql = "SELECT email FROM users WHERE email = :email";
        $query = $this->db->pdo->prepare($sql);
        $query->bindValue(':email', $email);
        $query->execute();

        if ($query->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getLoginUser(string $email, string $password)
    {
        $sql = "SELECT * FROM users WHERE email = :email AND password = :password LIMIT 1";
        $query = $this->db->pdo->prepare($sql);
        $query->bindValue(':email', $email);
        $query->bindValue(':password', $password);
        $query->execute();
        $result = $query->fetch(PDO::FETCH_OBJ);
        return $result;
    }

    public function login(array $data)
    {
        $email = $data['email'];
        $password = md5($data['password']);

        $checkEmail = $this->checkEmail($email);

        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $message = "<div class='alert alert-danger text-center'><strong>Hiba! </strong>Nem megfelelő e-mail formátum!</div>";
            return $message;
        }

        if($checkEmail == false) {
            $message = "<div class='alert alert-danger text-center'><strong>Hiba! </strong>Ez az e-mail cím nem létezik!</div>";
            return $message;
        }

        $result = $this->getLoginUser($email, $password);

        $loginMsg = "<div class='alert alert-success text-center'><strong>Sikeres bejelentkezés!</strong></div>";

        if ($result) {
            Session::init();
            Session::set("login", true);
            Session::set("name", $result->name);
            Session::set("login-msg", $loginMsg);
            header('Location: index.php');
        } else {
            $message = "<div class='alert alert-danger text-center'><strong>Hiba! </strong>Ezzel az adatokkal nincs felhasználó!</div>";
            return $message;
        }
    }
}