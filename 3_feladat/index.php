<?php
    include 'layouts/header.php';
    include 'lib/User.php';
    Session::checkSession();
    $user = new User();

    $loginMsg = Session::get('login-msg');
    if (isset($loginMsg)) {
        echo $loginMsg;
    }
    Session::set("login-msg", null);
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h2>Üdvözöljük <strong>
                <?php
                    $name = Session::get('name');
                    if (isset($name)) {
                        echo $name;
                    }
                ?>
            </strong></h2>
    </div>
</div>

<?php
    include 'layouts/footer.php';
?>