<?php
    include 'layouts/header.php';
    include 'lib/User.php';
    Session::checkLogin();
    $user = new User();
?>

<?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['login'])) {
        $loginUser = $user->login($_POST);
    }
?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>Bejelentkezés</h2>
        </div>

        <div class="panel-body" style="max-width: 600px;margin: 0 auto;">
            <?php if (isset($loginUser)) {
                echo $loginUser;
            }
            ?>
            <form action="" method="post">

                <div class="form-group">
                    <label for="email">E-mail cím</label>
                    <input type="text" id="email" name="email" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="password">Jelszó</label>
                    <input type="password" id="password" name="password" class="form-control" required>
                </div>

                <button type="submit" name="login" class="btn btn-success center-block">Bejelentkezés</button>
            </form>
        </div>
    </div>

<?php
include 'layouts/footer.php';
?>