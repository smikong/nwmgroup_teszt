<?php
    include 'layouts/header.php';
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h2>Sportesemény</h2>
    </div>
    <div class="panel-body">
        <?php
        $url = 'https://raw.githubusercontent.com/mefuru/OPTAVisualisation/master/f24.xml';
        $xml = simpleXML_load_file($url,"SimpleXMLElement",LIBXML_NOCDATA);
        if($xml ===  FALSE)
        {
            exit('Failed to open url.');
        }
        else {
            $result = $xml->xpath("//Event[@event_id='1']");
            $content = json_decode(json_encode($result),TRUE);
            print_r($content);
        }
        ?>
    </div>
</div>

<?php
    include 'layouts/footer.php';
?>
